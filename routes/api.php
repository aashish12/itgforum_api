<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//declearing the dingo api instance for the route

$api = app('Dingo\Api\Routing\Router');

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//by aashish
$api->version('v1', function($api){
    $api->group(['namespace' => 'App\Http\Controllers\Api'], function($api){

        $api->get('/users',['uses' => 'UserController@getUsers']);

        //api route to get all the responses related to address
        $api->group(['prefix' => 'address'], function($api){

            $api->get('/country', ['uses' => 'AddressController@getCountries']);

            $api->get('/province/{id}', ['uses' => 'AddressController@getProvinceByCountry']);
             $api->get('/provinces/{country}', ['uses' => 'AddressController@getProvinceByCountryName']);
            $api->get('/province', ['uses' => 'AddressController@getProvince']);

            $api->get('/zone/{id}', ['uses' => 'AddressController@getZoneByCountry']);
             $api->get('/zones/{country}', ['uses' => 'AddressController@getZoneByCountryName']);
            $api->get('/zone', ['uses' => 'AddressController@getZones']);

            $api->get('/district/{id}', ['uses' => 'AddressController@getDistrictByZone']);
            $api->get('/districts/{zone}', ['uses' => 'AddressController@getDistrictByZoneName']);
            $api->get('/district', ['uses' => 'AddressController@getDistricts']);

            $api->get('/city/{id}', ['uses' => 'AddressController@getCityByDistrict']);
            $api->get('/cities/{district}', ['uses' => 'AddressController@getCityByDistrictName']);
            $api->get('/city', ['uses' => 'AddressController@getCities']);

        });

        //route added by aashish for creating users ( testing purpose for jwt auth token)
         $api->group(['prefix' => 'users'], function($api){
             //api route to create user and login user
//             $api->post('signup',['uses' => 'UserController@signUpUsers']);

             //api route to register user from application form
             $api->post('/register',['uses' => 'UserController@registerUser']);
             $api->post('/signin',['uses' => 'UserController@signInUser']);
//             $api->get('update/password/{id}', ['uses' => 'UserController@changeUserPassword']);

             $api->get('/{id}', ['uses' => 'UserController@getUsersById']);

             $api->get('/{user_id}/activity/lists', ['uses' => 'PostController@getAllUserActivities']);
             //api related to skill of a user
             $api->get('/{id}/skills', ['uses' => 'UserController@getSkillsOfUser']);
             $api->post('/{id}/skill/create', ['uses' => 'UserController@createSkills']);

             //api related to experience of a users
             $api->get('/{id}/experience/detail', ['uses' => 'UserController@getExperienceOfUser']);
              $api->get('/{userid}/experience/{expid}', ['uses' => 'UserController@getSingleExperience']);//get single according to the id
             $api->put('/{userid}/experience/{expid}/update', ['uses' => 'UserController@updateExperienceOfUser']);//update
//             $api->delete('/{userid}/experience/{expid}/delete', ['uses' => 'UserController@deleteExperienceOfUser']);//delete the data
             $api->post('/{id}/experience/create', ['uses' => 'UserController@createExperience']);

             //api related to education of a users
             $api->get('/{id}/education/detail', ['uses' => 'UserController@getEducationOfUser']);
              $api->get('/{userid}/education/{expid}', ['uses' => 'UserController@getSingleEducation']);//get single according to the id
             $api->put('/{userid}/education/{expid}/update', ['uses' => 'UserController@updateEducationOfUser']);//update
//             $api->delete('/{userid}/education/{expid}/delete', ['uses' => 'UserController@deleteEducationOfUser']);//delete the data
             $api->post('/{id}/education/create', ['uses' => 'UserController@createEducation']);

             //api to get user projects
              $api->get('/{id}/projects', ['uses' => 'UserController@getProjectsByUser']);

             //only authenticated user can get all these functions added jwt auth middleware
             $api->group(['middleware' => 'jwt.auth'], function($api){

                 $api->post('/change/username/{id}', ['uses' => 'UserController@changeUsername']);
                 $api->post('/update/password/{id}', ['uses' => 'UserController@changeUserPassword']);
                 // $api->get('/{id}/projects', ['uses' => 'UserController@getProjectsByUser']);
//                 $api->get('/{id}', ['uses' => 'UserController@getUsersById']);
                 $api->get('/type/{id}',['uses'=>'UserController@getUsersByType']);

//                 $api->post('/{id}/skill/create', ['uses' => 'UserController@createSkills']);

                 //api route to add projects of authenticated users
                 $api->post('/project/add', ['uses' => 'UserController@postUserProjects']);

                 //logout route
                 $api->get('/logout', ['uses' => 'UserController@logout']);

             });

             //api route to get all the recent user activities
             $api->get('/{user_id}/activities', ['uses' => 'PostController@getAllUserActivities']);
         });

        //end of prefix users route group


        //this is to get specific forum post according to the id sent from frontend
        $api->get('/forum/{id}', ['uses' => 'PostController@getSelectedForumPost']);
        $api->get('/forum/category/{id}', ['uses' => 'PostController@getForumPostByCategory']);
        //api router for forum related functions
        $api->get('/forum',['uses' => 'PostController@getAllForumPosts']);

        //this is to post forum from client
        $api->post('/forum/post', ['uses' => 'PostController@insertForumPost']);

        //this is to post tht comment for a specific post
        $api->post('/forum/{post_id}/comment/create', ['uses' => 'PostController@postComment']);

         //api route for getting forum posts according to the category selected
        $api->get('/forum/category/{cat_id}', ['uses' => 'PostController@getAllCommentsOfPosts']);

        //this is to post tht comment for a specific post
        $api->get('/forum/{post_id}/comment/list', ['uses' => 'PostController@getAllCommentsOfPosts']);

        $api->get('/categories', ['uses' => 'PostController@getAllCategories']);

        //yo sabai kura haru authenticated user haru le matra garna paucha re
        $api->group(['prefix' => 'forum', 'middleware' => 'jwt.auth'], function($api){

            $api->get('/popular', ['uses' => 'PostController@getPopularForumPost']);
//            $api->get('/category', ['uses' => 'PostController@getAllCategories']);

            $api->group(['prefix' => 'post'], function($api){
                //this is to post forum from client
//                $api->post('/', ['uses' => 'PostController@insertForumPost']);

                //api route to post comment for a speciific forum post
                $api->post('/comment', ['uses' => 'PostController@postComment']);
            });



        });

        //api route for event related  functions
        $api->get('/events',['uses' => 'PostController@getAllEventPosts', 'as' => 'getAllEvents']);

        //api routes under event prefix
        $api->group(['prefix' => 'event'], function($api){
            $api->get('/{id}', ['uses' => 'PostController@getSingleEventbyId']);
        });

        //general apis

        //api route to search user with username, firstname, lastname, middlename
        $api->get('/search/user/{query}', ['uses' => 'SearchController@searchUser']);

        //api to search event
        $api->get('/search/event/{query}', ['uses' => 'SearchController@searchEvents']);

        //api to search event
        $api->get('/search/forum/{query}', ['uses' => 'SearchController@searchForumPosts']);


        $api->get('/language', ['uses' => 'PostController@getLanguages']);
        
        //api route to get all the members in out system
        $api->get('/members',['uses'=>'UserController@getMembers']);

        //api route to add feedback of different users
        $api->post('feedback/create', ['uses' => 'PostController@createFeedback']);
        $api->get('feedback/list', ['uses' => 'PostController@getAllFeedback']);

        //route to get all categorie
        $api->get('/categories', ['uses' => 'PostController@getAllCategories']);

        //api routes for admin part
        $api->group(['prefix' => 'admin'], function($api){
            $api->get('analytics', ['uses' => 'AdminController@getAdminAnalytics']);
            $api->get('verify/user/{id}', ['uses' => 'AdminController@verifyUser']);
            $api->get('users', ['uses' => 'AdminController@getUnverifiedUsers']);

            $api->group(['prefix' => 'pending'], function($api){
                $api->get('forum/posts', 'AdminController@getPendingForumPost');
            });
            $api->group(['prefix' => 'verify'], function($api){
                $api->get('forum/post/{id}', 'AdminController@verifyForumPost');
            });


            //api route related to events from users
            $api->group(['prefix' => 'event'], function($api){

                //api route to create event
                $api->post('/create', ['uses' => 'PostController@createEvent']);
                $api->get('/edit/{id}', ['uses' => 'PostController@editEvent']);

//                $api->get('/', function(){
//                    return route('getAllEvents');
//                });
            });
        });
    });

});

