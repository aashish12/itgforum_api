<?php namespace App;

/**
 * Eloquent class to describe the usertype_tbl table
 *
 * automatically generated by ModelGenerator.php
 */
class UsertypeTbl extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'usertype_tbl';

    protected $fillable = array('user_type');

    public function users()
    {
        return $this->hasMany('App\Users', 'user_type_id', 'id');
    }

}

