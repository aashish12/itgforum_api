<?php namespace App;

/**
 * Eloquent class to describe the transaction table
 *
 * automatically generated by ModelGenerator.php
 */
class Transaction extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'transaction';

    public $timestamps = false;

    protected $fillable = array('from', 'to', 'totalamount', 'remark', 'entrydate', 'paidamount', 'remainingamount');

    public function particular()
    {
        return $this->belongsTo('App\Particular', 'particularid', 'id');
    }

    public function userprofile()
    {
        return $this->belongsTo('App\Userprofile', 'userprofileid', 'id');
    }

    public function transactionstatus()
    {
        return $this->belongsTo('App\Transactionstatus', 'transactionstatusid', 'id');
    }
}
