<?php namespace App\models;

/**
 * Eloquent class to describe the experience_tbl table
 *
 * automatically generated by ModelGenerator.php
 */
class ExperienceTbl extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'experience_tbl';

    public function getDates()
    {
        return array('started_date', 'enddate_date');
    }

    protected $fillable = array('company_name', 'title', 'location', 'isworking', 'description', 'started_date',
        'enddate_date');

    public function users()
    {
        return $this->belongsTo('App\Users', 'users_id', 'id');
    }
}
