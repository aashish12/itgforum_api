<?php

namespace App\Http\Controllers\Api;

use App\models\CityTbl;
use App\models\CountryTbl;
use App\models\DistrictTbl;
use App\models\ProvinceTbl;
use App\models\ZoneTbl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class AddressController extends Controller
{
        /**
     * by aashish
     * returns all countries available
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCountries(){

        try{
            $countries = CountryTbl::all();

            if(count($countries) == 0){
                return response(['message' => 'Countries can not found']);
            }else{
                return response(['message' => 'List of countries', 'countries' => $countries]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something wwent wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }           
    }

    /**
     * by aashish
     * returns all province available
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getProvince(){
        try{
            $province = ProvinceTbl::all();

            if(count($province) == 0){
                return response(['message' => 'province can not found']);
            }else{
                return response(['message' => 'List of provinces', 'provinces' => $province]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aahsish
     * returns all zones available
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getZones(){
        try{
            $zones = ZoneTbl::all();

            if(count($zones) == 0){
                return response(['message' => 'Zones can not found']);
            }else{
                return response(['message' => 'List of zones', 'zones' => $zones]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns all districts available
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getDistricts(){
        try{
            $districts = DistrictTbl::all();

            if(count($districts) == 0){
                return response(['message' => 'Districts can not found']);
            }else{
                return response(['message' => 'List of districts', 'districts' => $districts]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns all cities available
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCities(){
        try{
            $cities = CityTbl::all();

            if(count($cities) == 0){
                return response(['message' => 'Cities can not found']);
            }else{
                return response(['message' => 'List of cities', 'countries' => $cities]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns all province according to the country id
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getProvinceByCountry($id){
        try{
            $response = DB::table('province_tbl')
                ->join('country_tbl', 'province_tbl.country_id', '=', 'country_tbl.id')
                ->select('province_tbl.id as province_id', 'province_tbl.province', 'country_tbl.country')
                ->where('province_tbl.country_id', '=', $id)
                ->get();
            if(count($response) == 0){
                return response(['message' => 'Province for this '.$id.' can not be found']);
            }else{
                return response(['message' => 'List of Province for '. $response[0]->country.' Found', 'province' => $response]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns all zones according to the country id
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getZoneByCountry($id){
        try{
            $response = DB::table('zone_tbl')
                ->join('country_tbl', 'zone_tbl.country_id', '=', 'country_tbl.id')
                ->select('zone_tbl.id as zone_id', 'zone_tbl.zone', 'country_tbl.country')
                ->where('zone_tbl.country_id', '=', $id)
                ->get();
            if(count($response) == 0){
                return response(['message' => 'Zones for this country id '.$id.' can not be found']);
            }else{
                return response(['message' => 'List of Zones for '. $response[0]->country.' Found', 'zones' => $response]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns all district according to the zone id
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getDistrictByZone($id){
        try{
            $response = DB::table('district_tbl')
                ->join('zone_tbl', 'district_tbl.zone_id', '=', 'zone_tbl.id')
                ->select('district_tbl.id as district_id', 'district_tbl.district', 'zone_tbl.zone')
                ->where('district_tbl.zone_id', '=', $id)
                ->get();
            if(count($response) == 0){
                return response(['message' => 'District for this Zone id '.$id.' can not be found']);
            }else{
                return response(['message' => 'List of Districts for '. $response[0]->zone.' Found', 'districts' => $response]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns all cities according to the district id
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getCityByDistrict($id){
        try{
            $response = DB::table('city_tbl')
                ->join('district_tbl', 'city_tbl.district_id', '=', 'district_tbl.id')
                ->select('city_tbl.id as city_id', 'city_tbl.city', 'district_tbl.district')
                ->where('city_tbl.district_id', '=', $id)
                ->get();
            if(count($response) == 0){
                return response(['message' => 'Cities for this District id '.$id.' can not be found']);
            }else{
                return response(['message' => 'List of Cities for '. $response[0]->district.' Found', 'cities' => $response]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

     public function getProvinceByCountryName($country){
        try{
            $response = DB::table('country_tbl')
                ->join('province_tbl', 'province_tbl.country_id', '=', 'country_tbl.id')
                ->select('province_tbl.id as province_id', 'province_tbl.province', 'country_tbl.country')
                ->where('country_tbl.country', '=', $country)
                ->get();
            if(count($response) == 0){
                return response(['message' => 'Province for this '.$country.' can not be found']);
            }else{
                return response(['message' => 'List of Province for '. $response[0]->country.' Found', 'countries' => $response]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    public function getZoneByCountryName($country){
        try{

//            return response(['data' => $country]);
            $response = DB::table('zone_tbl')
                ->join('country_tbl', 'zone_tbl.country_id', '=', 'country_tbl.id')
                ->select('zone_tbl.id as zone_id', 'zone_tbl.zone', 'country_tbl.country')
                ->where('country_tbl.country', '=', $country)
                ->get();
            if(count($response) == 0){
                return response(['message' => 'Zones for this country id '.$country.' can not be found']);
            }else{
                return response(['message' => 'List of Zones for '. $response[0]->country.' Found', 'zones' => $response]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

        public function getDistrictByZoneName($zone){
        try{
            $response = DB::table('zone_tbl')
                ->join('district_tbl', 'district_tbl.zone_id', '=', 'zone_tbl.id')
                ->select('district_tbl.id as zone_id', 'district_tbl.district', 'zone_tbl.zone')
                ->where('zone_tbl.zone', '=', $zone)
                ->get();
            if(count($response) == 0){
                return response(['message' => 'District for this '.$zone.' zone can not be found']);
            }else{
                return response(['message' => 'List of Districts for '. $response[0]->zone.' Found', 'districts' => $response]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    public function getCityByDistrictName($district){
        try{
            $response = DB::table('district_tbl')
                ->join('city_tbl', 'city_tbl.district_id', '=', 'district_tbl.id')
                ->select('city_tbl.id as city_id', 'city_tbl.city', 'district_tbl.district')
                ->where('district_tbl.district', '=', $district)
                ->get();
            if(count($response) == 0){
                return response(['message' => 'Cities for this  '.$district.'District can not be found']);
            }else{
                return response(['message' => 'List of Cities for '. $response[0]->district.' Found', 'cities' => $response]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }
}
