<?php

namespace App\Http\Controllers\Api;



use App\models\AddressTbl;
use App\CourseTbl;
use App\models\ExperienceTbl;
use App\models\EducationTbl;
use App\models\InternProjectTbl;
use App\models\EventTbl;
use App\models\PostTbl;
use App\models\UserinfoTbl;
use App\models\SkillTbl;
use App\User;
use App\models\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use DB;

class UserController extends Controller
{
    //
    /**
     * By aashish
     * returns list of all registered users
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers(){

        try{
            $allusers = User::all();
//            return response()->json(['message' => 'operation successfull', 'users' => $allusers],200);
            return $allusers;

        }catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }


    /**
     * by aashish
     * @param Request $request
     * @return array
     * this function is for creating new user with token
     */
    public function signUpUsers(Request $request){

        $user = new User([
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'user_type_id' => $request->input('usertype_id'),//4-intern
            'status_id' => $request->input('status_id'),//0-active
        ]);

        $user->save();

        return array(['message' => $user, 'status' => 201]);

    }


    /**
     * by aashish
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * function for checking the user so that we can generate jwt token
     */
    public function signInUser(Request $request){

        $email = $request->input('email');
        $credentials = $request->only('email', 'password');
        $users = DB::table('users')
            ->select('users.id as user_id', 'users.username', 'users.email', 'users.user_type_id', 'users.status_id')
            ->where('users.email', $email)
            ->get();

        $user_id = $users[0]->user_id;
        $username = $users[0]->username;
//        return response()->json(['message' => $credentials, 'users' => $users, 'user_id' => $user_id, 'username' => $username]);

        try{
            if(! $token = JWTAuth::attempt($credentials)){
                return response()->json([
                   'error' => 'Invalid Credentials !'
                ], 401);
            }
        }catch(JWTException $e){
            return response()->json([
                'error' => 'Could not create token !!! sorry'
            ], 500);
        }

        return response()->json([
           'token' => $token,
            'user_id' => $user_id,
            'username' => $username
        ], 200);
    }

    /**
     * by sabin
     * return the numbers of registered users in the system
     * @return \Illuminate\Http\JsonResponse
     */
    public function countUsers(){
        try{
            $users = Users::countUsers();
            return response()->json(['message'=>'operation success', 'users'=>$users, 'status'=>'200']);
        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * by sabin
     * gets user according to their specific id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersById($id){
        try{
            $users = Users::getUsersAccToId($id);
//            $gender = $users->gender;
            if($users[0]->gender == "female"){
                $imageLocation = asset('/users/avatar');//.'/users/avatar/avatar_female.png';//.$users[0]->profile_image;
//                return response()->json(['profile_image' => $imageLocation]);
            }elseif($users[0]->gender == "male"){
                $imageLocation = asset('/users/avatar');//.$users[0]->profile_image;
//                return response()->json(['profile_image' => $imageLocation]);
            }
//            return response()->json(['gender' => $users[0]->gender]);
            if(count($users)<0){
                return response()->json('no users data found');
            }
            else{
                return response()->json(['message' => 'operation successfull', 'users' => $users, 'profile_image' => $imageLocation, 'status'=>200]);
            }

        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }

    }

    /**
     * by sabin
     * get all projects of user
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjectsByUser($id){
        try{
//            return response()->json(['id' => $id]);
            $projects = Users::getProjectsAccToUser($id);
            if(count($projects) == 0){
                return response(['message'=>'No projects Found', 'status' => 503]);    
            }else{
                return response(['message'=>'Lists of project for user','projects'=>$projects,'status'=>200]);
            }
            
        }catch(\Exception $e){
            return response(['message' => $e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by sabin
     * gets user by their type(admin, mentor, intern)
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersByType($id){
        try{
            $users = Users::getUsersAccToType($id);
            return response()->json(['message' => 'operation successful', 'users' => $users, 'status'=>200]);

        }catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * by sabin
     * get members according to name (user type)
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMembers(){
        try{
//            return response()->json(['message' => 'This is get Members functions']);

            $founders = Users::getFounders();
            $mentors = Users::getMentors();
            $sub_mentors = Users::getSubMentors();
            $members_only = Users::getMembersOnly();

            // return response(['message' => $members_only]);

            if($members_only[0]->gender == "female"){
                $imageLocation = asset('/users/avatar');//.'/users/avatar/avatar_female.png';//.$users[0]->profile_image;
//                return response()->json(['profile_image' => $imageLocation]);
            }elseif($members_only[0]->gender == "male"){
                $imageLocation = asset('/users/avatar');//.$users[0]->profile_image;
//                return response()->json(['profile_image' => $imageLocation]);
            }

            return response()->json(['message' => 'operation successful',
                'founders' => $founders,
                'mentors' => $mentors,
                'submentors' => $sub_mentors,
                'membersonly' => $members_only,
                'member_profile' => $imageLocation,
                'status'=>200]);

        }catch(Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * by aashish
     * post function for user registration
     * @param Request $request
     * @return array
     */
    public function registerUser(Request $request){

        try{
            $userinfoObj = new UserinfoTbl();
            $userObj = new Users();
            $addressObj = new AddressTbl();

            $userObj->email = $request->get('email');
            $userObj->user_type_id = 4;
            $userObj->status_id = 3;

            $userObj->save();

            if($userObj->save()){
                $userTbl_id = $userObj->id;
            }else{
                return array(['message' => 'User is not intserted into users table']);
            }

            $addressObj->country_id = $request['country_id'];
            $addressObj->province_id = $request['province_id'];
            $addressObj->zone_id = $request['zone_id'];
            $addressObj->district_id = $request['district_id'];
            $addressObj->city_id = $request['city_id'];
            $addressObj->local_address = $request['local_address'];

            $addressObj->save();
            if($addressObj->save()){
                $addressTbl_id = $addressObj->id;
            }else{
                return array(['message' => 'User is not intserted into users table']);
            }

            $userinfoObj->fname = $request['fname'];
            $userinfoObj->user_id = $userTbl_id;
            $userinfoObj->lname = $request['lname'];
            $userinfoObj->mname = $request['mname'];
            $userinfoObj->mobile_no = $request['mobile_no'];
            $userinfoObj->email = $request['email'];
//            $userinfoObj->course_type_id = null;
            $userinfoObj->gender = $request['gender'];

            $userinfoObj->gender = $request['gender'];
            if($request['gender'] === "male"){
                $userinfoObj->profile_image = 'avatar-m.png';
            }elseif($request['gender'] === "female"){
                $userinfoObj->profile_image = 'avatar-f.png';
            }else{
                $userinfoObj->profile_image = 'avatar-m.png';
            }

            $userinfoObj->dob = $request['dob'];
            $userinfoObj->address_id = $addressTbl_id;
            $userinfoObj->course_id = $request['course_id'];//course id must
            $userinfoObj->skill_id = null;//skill id must be updated when user add new skills
            $userinfoObj->created_at = Carbon::now();
            $userinfoObj->updated_at = Carbon::now();
            $userinfoObj->college = $request['college'];
            $userinfoObj->whywe = $request['whywe'];
//            $userinfoObj->language_type_id = null;

            $userinfoObj->save();
            if($userinfoObj->save()){
                $userinfoTbl_id = $userinfoObj->id;
                return array(['message' => 'User is intserted into users table', 'transaction_id' => $userinfoTbl_id]);
            }else{
                return array(['message' => 'User is not intserted into users table']);
            }

        }catch(\Exception $e){
            return array(['message' => $e->getMessage(), 'status' => $e->getCode()]);
        }

        return array(['users' => $userObj, 'users_info' => $userinfoObj, 'address_info' => $addressObj]);
    }


    /**
     * by aashish
     * adding new projects of users
     * @param Request $request
     * @param $id
     * @return array
     */
    public function postUserProjects(Request $request){
        try{
            $userProject = new InternProjectTbl();
            $userProject->project_title = $request['project_title'];
            $userProject->project_description = $request['project_description'];
            $userProject->project_duration = $request['project_duration'];
            $userProject->project_status = $request['project_status'];
//            $userProject->project_started_date = $request['project_started_date'];
            $userProject->user_id = $request['user_id'];
            $userProject->status_id = $request['status_id'];
            $userProject->save();

            if(!$userProject->save()){
                return response()->json(['message' => 'data can not be saved']);
            }else{
                return response()->json(['projects' => $userProject]);
            }

        }catch(\Exception $e){
            return response()->json(['message' => 'Something went wrong '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }


    public function changeUsername(Request $request, $id){
        try{
            $updateUsername = Users::find($id);
            $updateUsername->username = $request->input('username');
            $updateUsername->updated_at = Carbon::now();
            $updateUsername->save();

            if($updateUsername->save()){
                return response()->json(['message' => 'Username Updated ', 'users' => $updateUsername]);
            }else{
                return response()->json(['message' => 'Username Not Updated ']);
            }
        }catch(\Exception $e){
            return response()->json(['message' => 'Something Went Wrong!!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * to add the skill of a specific user
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createSkills(Request $request, $id){
        try{
            $skillObj = new SkillTbl();
            $skillObj->skills = $request->get('skill');
            $skillObj->user_id = $id;
            $skillObj->created_at = Carbon::now();
            $skillObj->updated_at = Carbon::now();
            $skillObj->save();

            if($skillObj->save()){
                return response(['message' => 'Your skill is been added', 'skills' => $skillObj]);
            }else{
                return response(['message' => 'Something Problem in adding your skill']);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something Went wrong '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns lists of skills of a specific users 
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getSkillsOfUser($id){
        try{
            $result = SkillTbl::where('user_id', '=', $id)->get();
            if(count($result) <= 0){
                return response(['message' => 'No skills added !! please add some']);
            }else{
                return response(['message' => 'Lists of skills for users', 'skills' => $result]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something Went wrong '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * add experience of user
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createExperience(Request $request, $id){
        try{
            $experienceObj = new ExperienceTbl();
            $experienceObj->company_name = $request->get('company_name');
            $experienceObj->title = $request->get('position'); //title vaneko position nai ho
            $experienceObj->location = $request->get('company_location');
            $experienceObj->isworking = $request->get('working_status');
            $experienceObj->description = $request->get('job_description');
            $experienceObj->started_date = $request->get('started_date');
            $experienceObj->end_date = $request->get('end_date');
            $experienceObj->user_id = $id;
            $experienceObj->created_at = Carbon::now();
            $experienceObj->updated_at = Carbon::now();

            $experienceObj->save();

            if($experienceObj->save()){
                return response(['message' => 'Your Data is successfully stored', 'experience' => $experienceObj]);
            }else{
                return response(['message' => 'Data can not be inserted']);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something Went wrong '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns all the list of experiences of users
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getExperienceOfUser($id){
        try{
            $result = ExperienceTbl::where('user_id', '=', $id)
                ->orderBy('created_at', 'DESC')
                ->get();
            if(count($result) <= 0){
                return response(['message' => 'No Experience added !! please add some']);
            }else{
                return response(['message' => 'Lists of Experiences for user', 'experience' => $result]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something Went wrong '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }


    /**
     * @param $userid
     * @param $expid
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getSingleExperience($userid, $expid){
        try{
            $result = DB::table('experience_tbl')
                ->select('id as experience_id', 'company_name', 'title', 'description', 'isworking', 'location')
                ->where('user_id', '=', $userid)
                ->where('id', '=', $expid)
                ->get();

            if(count($result) == 0){
                return response(['message' => 'No Data found']);
            }else{
                return response(['message' => 'Found Information !!!', 'datas' => $result, 'status' => 200]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * @param Request $request
     * @param $userid
     * @param $expid
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateExperienceOfUser(Request $request, $userid, $expid){
        try{
            $experienceObj = ExperienceTbl::find($expid);
            $experienceObj->company_name = $request->get('company_name');
            $experienceObj->title = $request->get('position'); //title vaneko position nai ho
            $experienceObj->location = $request->get('company_location');
            $experienceObj->isworking = $request->get('working_status');
            $experienceObj->description = $request->get('job_description');
            $experienceObj->started_date = $request->get('started_date');
            $experienceObj->end_date = $request->get('end_date');
            // $experienceObj->created_at = Carbon::now();
            $experienceObj->updated_at = Carbon::now();

            $experienceObj->save();

            if($experienceObj->save()){
                return response(['message' => 'Your Data is successfully Updated !!!', 'datas' => $experienceObj, 'status' => 200]);
            }else{
                return response(['message' => 'Data can not be inserted']);
            }

        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * to add education detail of users
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createEducation(Request $request, $id){
        try{
            $educationObj = new EducationTbl();
            $educationObj->institution = $request->get('institute');
            $educationObj->degree_level = $request->get('degree');
            $educationObj->grades = $request->get('grades');
            $educationObj->started_date = $request->get('started_date');
            $educationObj->end_date = $request->get('end_date');
            $educationObj->description = $request->get('description');
            $educationObj->user_id = $id;
            $educationObj->created_at = Carbon::now();
            $educationObj->updated_at = Carbon::now();

            $educationObj->save();

            if($educationObj->save()){
                return response(['message' => 'Your Data is successfully stored', 'datas' => $educationObj]);
            }else{
                return response(['message' => 'Data can not be inserted']);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something Went wrong '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getEducationOfUser($id){
        try{
            $result = EducationTbl::where('user_id', '=', $id)->get();
            if(count($result) <= 0){
                return response(['message' => 'No Education Detail Found !! please add some']);
            }else{
                return response(['message' => 'Lists of Education Detail for user', 'datas' => $result]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * @param $userid
     * @param $expid
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getSingleEducation($userid, $expid){
        try{
            $result = DB::table('education_tbl')
                ->select('id as education_id', 'institution', 'degree_level', 'major_study_area', 'grades', 'description')
                ->where('user_id', '=', $userid)
                ->where('id', '=', $expid)
                ->get();

            if(count($result) == 0){
                return response(['message' => 'No Data found']);
            }else{
                return response(['message' => 'Found Information !!!', 'datas' => $result, 'status' => 200]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * @param Request $request
     * @param $userid
     * @param $expid
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateEducationOfUser(Request $request, $userid, $expid){
        try{
            $educationObj = EducationTbl::find($expid);
            $educationObj->institution = $request->get('institute');
            $educationObj->degree_level = $request->get('degree');
            $educationObj->major_study_area = $request->get('major_subject');
            $educationObj->grades = $request->get('grades');
            $educationObj->started_date = $request->get('started_date');
            $educationObj->end_date = $request->get('end_date');
            $educationObj->description = $request->get('description');
            $educationObj->updated_at = Carbon::now();

            $educationObj->save();

            if($educationObj->save()){
                return response(['message' => 'Your Data is successfully Updated !!!', 'datas' => $educationObj, 'status' => 200]);
            }else{
                return response(['message' => 'Data can not be updated']);
            }

        }catch(\Exception $e){
            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }


    public function changeUserPassword(Request $request, $id){
        try{
//            $users = DB::table('users')
//                ->select('username', 'email', 'password')
//                ->where('id', '=', $id)
//                ->get();
            $users = Users::find($id);
            return response()->json(['message' => 'User Found', 'users' =>$users]);

            if($users[0]->username == $request->get('username')){
                return response()->json(['message' => 'username matched']);
            }

            return response()->json(['message' => 'users password is been updated successfully',
                'olduserinfo' => $users[0]->username,
                'oldusername' => $request->input('username'),
                'oldpassword' => $request->input('password')
            ]);
        }catch(\Exception $e){
            return response()->json(['message' => 'Something went wrong '. $e->getMessage(), 'status' => $e->getCode()]);
        }
    }
}
