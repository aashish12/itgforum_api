<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;

class SearchController extends Controller
{
    //

    public function searchUser($query){

//        return response(['message' => $query]);
        try{

            if($query == ''){
              return response(['message' => 'No Result Found', 'status' => 503]);
            }

            $results = DB::table('users')
                ->join('status_tbl', 'users.status_id', '=', 'status_tbl.id')
                ->join('usertype_tbl', 'users.user_type_id', '=', 'usertype_tbl.id')                    //user-type-tbl join
                ->join('userinfo_tbl', 'users.id', '=' , 'userinfo_tbl.user_id')                        //user info table
                ->select(
                    'users.id as user_id', 'users.user_type_id as user_typeid',
                    'usertype_tbl.user_type as usertype_name', 'users.status_id',
                    'status_tbl.status as status_name',
                    'userinfo_tbl.fname', 'userinfo_tbl.mname',
                    'userinfo_tbl.lname','userinfo_tbl.gender',
                    'userinfo_tbl.profile_image'
                )
//                ->where('users.user_type_id', '=', 4)
                ->orWhere('userinfo_tbl.fname', 'LIKE', "%$query%")
                ->orWhere("userinfo_tbl.mname", "LIKE", "%$query%")
                ->orWhere("userinfo_tbl.lname", "LIKE", "%$query%")
                ->orWhere("users.username", "LIKE", "%$query%")
//                ->where('status_tbl.id', 4)
//                ->orderBy('users.id', 'DESC')
                ->get();

           foreach($results as $key => $result){
               if($result->gender == "female"){
                   $imageLocation = asset('/users/avatar');//.'/users/avatar/avatar_female.png';//.$users[0]->profile_image;
                   $results[$key]->imagelocation = $imageLocation;
//                return response()->json(['profile_image' => $imageLocation]);
               }elseif($result->gender == "male"){
                   $imageLocation = asset('/users/avatar');//.$users[0]->profile_image;
                   $results[$key]->imagelocation = $imageLocation;
//                return response()->json(['profile_image' => $imageLocation]);
               }
           }

            if(count($results) == 0){
                return response(['message' => 'Empty Search result', 'status' => 503]);
            }else{
                return response(['message' => 'User Found', 'datas' => $results, 'status' => 200]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something Wend Wrong !! Error '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns events according to the search paramenter
     * @param $query
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function searchEvents($query){
        try{
            $results = DB::table('event_tbl')
                ->join('users', 'event_tbl.user_id', '=', 'users.id')
                ->join('status_tbl', 'event_tbl.status_id', '=', 'status_tbl.id')
                ->select('event_tbl.id as event_id', 'event_tbl.event_title', 'event_tbl.event_description','event_tbl.event_image',
                    'event_tbl.event_location', 'event_tbl.start_datetime', 'event_tbl.end_datetime',
                    'event_tbl.created_at','event_tbl.event_title', 'event_tbl.created_at',
                    'users.id as user_id', 'users.username'
                )
                ->where('event_tbl.event_title', 'LIKE', "%$query%")
//                ->orderBy('created_at', 'DESC')
                ->get();

            foreach($results as $key => $data){
                $postedtime = $data->created_at;
                $differenttime = Carbon::parse($postedtime);
                $results[$key]->postedtime = $differenttime->diffForHumans();
            }

            if(count($results) == 0){
                return response(['message' => 'Result Cannot found !! ', 'status' => 503]);
            }else{
                return response(['message' => 'Results Found', 'datas' => $results, 'status' => 503]);
            }
        }catch(\Exception $e){

        }
    }

        /**
     * by aashish
     * returns forum posts according to the query user request
     * @param $query
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function searchForumPosts($query){

        try{
//            if($query == ''){
//                return response(['message' => 'No parameter is passed and the search query is : '.$query]);
//            }

            $results = DB::table('post_tbl')
                ->join('users', 'post_tbl.user_id', '=' , 'users.id' )
                ->join('category_tbl', 'post_tbl.category_id', '=' , 'category_tbl.id' )
                ->join('userinfo_tbl', 'userinfo_tbl.user_id', '=' , 'users.id' )
                ->select(
                    'post_tbl.id as post_id','post_tbl.post_title','post_tbl.post_body','post_tbl.created_at', 'post_tbl.post_code',
                    'category_tbl.id as cat_id','category_tbl.category',
                    'users.username as username','users.email', 'users.id as user_id',
                    'userinfo_tbl.fname','userinfo_tbl.lname','userinfo_tbl.profile_image', 'userinfo_tbl.gender'
                )
//                ->where('post_tbl.status_id', 6)
                ->orWhere('post_tbl.post_title', 'LIKE', "%$query%")
                ->orderBy('post_tbl.created_at', 'DESC')
                ->get();

            foreach($results as $key => $data){

                if($data->gender == "female"){
                    $results[$key]->profile_imagelink = asset('/users/avatar/');
                }elseif($data->gender == "male"){
                    $results[$key]->profile_imagelink = asset('/users/avatar/');
                }

                $postedtime = $data->created_at;
                $differenttime = Carbon::parse($postedtime);
                $results[$key]->postedtime = $differenttime->diffForHumans();
            }


            if(count($results) == 0){
                return response(['message' => 'Result Cannot found !! ', 'status' => 503]);
            }else{
                return response([
                  'message' => 'Results Found for search query !! Keyword = '.$query, 
                  'datas' => $results,
                  'status' => 503]);
            }

        }catch(\Exception $e){
            return response([
                'message' => 'Something went wrong !! Error: '.$e->getMessage(),
                'status' => $e->getCode()
            ]);
        }
    }
}
