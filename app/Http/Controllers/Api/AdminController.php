<?php

namespace App\Http\Controllers\Api;

//use App\models\UserinfoTbl;
//use App\models\Users;
use App\models\CommentTbl;
use App\Mail\UserEmailService;
use App\models\EventTbl;
use App\models\PostTbl;
use App\models\UserinfoTbl;
use App\models\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Mail;


class AdminController extends Controller
{

    public function verifyUser($id){
        try{
            $user = Users::find($id);
            $userinfoDetail = UserinfoTbl::where('user_id', '=', $id)->first();
//            return response()->json(['users' => $user, 'countuser' => count($user), 'user_info' => $userinfoDetail]);

            if(count($user) == 0 ){
                return response()->json(['message' => 'Users not found']);
            }else{

               $userEmail = $userinfoDetail->email;
                $firstName = $userinfoDetail->fname;
                $lastName = $userinfoDetail->lname;
                    $string=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9','@','#','&','*');
                $id_array=array_rand($string,8);
                $id='';
                foreach($id_array as $key)
                {
                    $id.=$string[$key];
                }
                $newUsername = $lastName.$firstName.rand();
                $newPassword =$id;

                //this boject for sending to the mail class to send mail to user
                $users = [
                    'loginusername' => $newUsername,
                    'loginemail' => $userEmail,
                    'loginpassword' => $newPassword
                    ];

                if($user->status_id == 3){
//                    return response()->json(['message' => 'Users not found', 'user_status' => $user, $userinfoDetail]);

                    $user->username = $newUsername;
                    $user->password = bcrypt($newPassword);
                    $user->status_id = 4; //four is to active user
                    $user->user_type_id = 4; // 4 is for normal users
                    $user->updated_at = Carbon::now();
                    $user->save();

                    $when = Carbon::now()->addMinutes(5);

                    Mail::to($userEmail)
                        ->cc('info@itglance.com.np')
                        ->bcc('sbinmaharjan@gmail.com', 'network@itglance.com.np')
                        ->later($when, new UserEmailService($users));

                    return response()->json([
                        'message' => 'User is being approved !! check your email for further detail.',
                        'userinfo' => $user,
                        'newusername' => $newUsername
                    ]);
                }else{
                    return response()->json(['message' => 'user is already active']);
                }
                // now userko status inactive bata active ma change garna and then user ko first name
                // and lastname am random number use garera username and password set garnaa
                //Then tyo random username and password lae user ko ma mail garne and return the result
            }
        }catch(\Exception $e){
            return response()->json(['message' => 'something went wrong '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    public function getUnverifiedUsers(){
        try{
//            $users = Users::where('status_id', 1); //district
            $users = Users::select(
                'users.id as user_id','users.username', 'users.password', 'users.status_id', 'users.user_type_id', 'users.email'
//                'userinfo_tbl.fname', 'userinfo_tbl.lname', 'userinfo_tbl.mname'
                )
//                ->join('userinfo_tbl', 'userinfo_tbl.user_id', '=', 'users.id')
                ->where('users.status_id', 3)
                ->orderBy('users.created_at', 'DESC')
                ->get();

//            return response()->json(['message' => 'List of users', 'users' => $users]);
//            $users = DB::table('users')
//                ->join('userinfo_tbl', 'userinfo_tbl.user_id', '=', 'users.id')
//                ->select(
//                        'users.id as user_id','users.username', 'users.password', 'users.status_id', 'users.user_type_id', 'users.email',
//                        'userinfo_tbl.fname', 'userinfo_tbl.lname', 'userinfo_tbl.mname'
//                    )
//                ->where('users.status_id', '=', 3)
//                ->orderBy('users.created_at', 'DESC')
//                ->get();

//            return
            if(count($users) == 0 ){
                return response()->json(['message' => 'Users not found']);
            }else{
                return response()->json(['message' => 'List of users', 'users' => $users]);
            }
        }catch(\Exception $e){
            //print_r($e->getMessage()); die();
            return response()->json([
                'message' => 'something went wrong '. $e->getMessage(),
                'status' => $e->getCode()
            ]);
        }
    }

    public function getPendingForumPost(){
        try{
            $posts = PostTbl::select('id as post_id', 'post_title', 'post_body', 'category_id')
                ->where('status_id', 5)
                ->orderBy('created_at', 'DESC')
                ->get();
            if(count($posts) == 0 ){
                return response()->json(['message' => 'No any pending posts']);
            }else{
                return response()->json(['message' => 'List of pending posts', 'posts' => $posts]);
            }
        }catch(\Exception $e){
            return response()->json([
                'message' => 'something went wrong '. $e->getMessage(),
                'status' => $e->getCode()
            ]);
        }
    }

    public function verifyForumPost($id){
        try{
            $post = PostTbl::find($id);

            if($post->status_id == 6){
                return response()->json(['message' => 'This post is already active', 201]);
            }
            $post->status_id = 6;
            $post->save();
            return response()->json(['message' => 'Congratulation !! This post is been approved', 'post' => $post]);

        }catch(\Exception $e){
            return response()->json([
                'message' => 'something went wrong '. $e->getMessage(),
                'status' => $e->getCode()
            ]);
        }
    }

    public function getAdminAnalytics(){
        try{
            $users = Users::count();
            $posts = PostTbl::count();
            $comments = CommentTbl::count();
            $events = EventTbl::count();

            return response()->json([
                'message' => 'Analytics for admin',
                'users' => $users,
                'posts' => $posts,
                'comments' => $comments,
                'events' => $events
            ]);
        }catch(\Exception $e){
            return response()->json([
                'message' => 'something went wrong '. $e->getMessage(),
                'status' => $e->getCode()
            ]);

        }
    }

    /**
    * by aashish
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getAllFeedback(){
        try{
            $result = DB::table('feedback_tbl')
                ->select('id', 'title', 'username', 'body', 'status', 'created_at', 'updated_at')
                ->orderBy('created_at', 'DESC')
                ->get();

            $countfeedback = count($result);

            foreach($result as $key => $data){
                $postedtime = $result[$key]->created_at;
                $differenttime = Carbon::parse($postedtime);
                $result[$key]->postedtime = $differenttime->diffForHumans();
            }

            if(count($result) == 0){
                return response(['message' => 'No any feedback for now']);
            }else{
                return response(['message' => 'List of feedback', 'datas' => $result, 'countfeedback' => $countfeedback, 'status' => 200]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something Went Wrong !! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }
}
