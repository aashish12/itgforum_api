<?php



namespace App\Http\Controllers\Api;



use App\models\CategoryTbl;
use App\models\FeedbackTbl;


use App\models\CommentTbl;

use App\models\EventTbl;

use App\models\PostTbl;

use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Event;

use Mockery\CountValidator\Exception;

use Tymon\JWTAuth\Facades\JWTAuth;

use DB;



class PostController extends Controller

{



    /**

     * by aashish

     * returns lists of all posts (forum post) and their detail

     * @return \Illuminate\Http\JsonResponse

     */

    public function getAllForumPosts(Request $request){


        // if(! $user = JWTAuth::parseToken()->authenticate()){

        //     return response()->json([

        //         'message' => 'Not Authenticated User'

        //     ], 404);

        // }

        try{

        	// $results = PostTbl::all();
             $results = DB::table('post_tbl')
                ->join('users', 'post_tbl.user_id', '=' , 'users.id' )
                ->join('category_tbl', 'post_tbl.category_id', '=' , 'category_tbl.id' )
                ->join('userinfo_tbl', 'userinfo_tbl.user_id', '=' , 'users.id' )
                // ->join('comment_tbl', 'comment_tbl.post_id', '=', 'post_tbl.id')
                ->select(
                    'post_tbl.id as post_id','post_tbl.post_title','post_tbl.post_body','post_tbl.created_at', 'post_tbl.post_code',
                    'category_tbl.id as cat_id','category_tbl.category',
                    'users.username as username','users.email', 'users.id as user_id',
                    'userinfo_tbl.fname','userinfo_tbl.lname','userinfo_tbl.profile_image', 'userinfo_tbl.gender'
                )
                ->where('post_tbl.status_id', '=', '3')
                ->orderBy('created_at', 'DESC')
                ->get();

            //getting recent data from the table

            // $datas = PostTbl::getAllPosts();
            // $countcomment = CommentTbl::countComments();
            // foreach($countcomment as $key => $countcmnt){
            //     $results[$key]->countcomment = $countcmnt;
            // }

            $imageLocation = '';

            
            foreach($results as $key => $data){

                if($data->gender == "female"){
                    $results[$key]->profile_imagelink = asset('/users/avatar/');
                }elseif($data->gender == "male"){
                    $results[$key]->profile_imagelink = asset('/users/avatar/');
                }

                $postedtime = $data->created_at;
                $differenttime = Carbon::parse($postedtime);
                $results[$key]->postedtime = $differenttime->diffForHumans();
            }


            if(count($results) == 0){

                return response()->json(['message' => 'Nothing to show', 'status' => 204]);

            }else{

                return response()->json([
                    'message' => 'operation successfull', 
                    'posts' => $results,
                    'status' => 200
                    ]);

            }

        }catch(\Exception $e){

            return response()->json(['code' => $e->getCode(), 'message' => $e->getMessage()]);

        }



    }



    /**

     *

     * by aashish

     * return post lists according to the category

     *

     */

    public function getForumPostByCategory($cat_id){



        //before adding auth.jwt middleware in route we have used this mechanish to parse user token

//        if(! $user = JWTAuth::parseToken()->authenticate()){

//            return response()->json([

//                'message' => 'Not Authenticated User'

//            ], 404);

//        }

        //after adding the middleware to the route we use this technique to get user token

        // $user = JWTAuth::parseToken()->toUser();

        try{

            $results = PostTbl::getForumPostAccToCategory($cat_id);

            // return response(['message' => $results]);
            $imageLocation = '';

            
            foreach($results as $key => $data){

                if($data->gender == "female"){
                    $results[$key]->profile_imagelink = asset('/users/avatar/');
                }elseif($data->gender == "male"){
                    $results[$key]->profile_imagelink = asset('/users/avatar/');
                }

                $postedtime = $data->created_at;
                $differenttime = Carbon::parse($postedtime);
                $results[$key]->postedtime = $differenttime->diffForHumans();
            }


            if(count($results) == 0){
                return response(['message' => 'No data found', 'status' => 503]);
            }else{
                return response([
                    'message' => 'List of forum posts according to category',
                    'posts' => $results,
                    'status' => 200
                    ]);
            }

        }catch(\Exception $e){

            return response(['message' => 'Something went wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);

        }



    }



    public function getForumCategory(){



        try{

            $returndata = PostTbl::getForumPostCategory();



            if(count($returndata)<=0){

                return response()->json(['message' => 'There is no category lists', 'status' => 250]);

            }else{

                return response()->json(['message' => 'Success', 'datas' => $returndata]);

            }



        }catch(\Exception $e){

            return response()->json(['code' => $e->getCode(), 'message' => $e->getMessage()]);

        }



    }



    public function getPopularForumPost(){

        try{

            $returndata = CommentTbl::countComments();

            if(count($returndata)<=0){

                return response()->json(['message' => 'There is no category lists', 'status' => 250]);

            }else{

                return response()->json(['message' => 'Success', 'datas' => $returndata]);

            }


        }catch(\Exception $e){

            return response()->json(['code' => $e->getCode(), 'message' => $e->getMessage()]);

        }

    }


    public function getSelectedForumPost($id){



        try{

            $singlePost = PostTbl::getSelectedForum($id);



            $allcomment = CommentTbl::getCommentAccToPost($id);

            $imageLocation = '';
            
            // foreach($results as $key => $data){

                if($singlePost[0]->gender == "female"){
                    $singlePost[0]->profile_imagelink = asset('/users/avatar/');
                }elseif($singlePost[0]->gender == "male"){
                    $singlePost[0]->profile_imagelink = asset('/users/avatar/');
                }

                $postedtime = $singlePost[0]->created_at;
                $differenttime = Carbon::parse($postedtime);
                $singlePost[0]->postedtime = $differenttime->diffForHumans();
            // }

            foreach($allcomment as $key => $data){

                if($data->gender == "female"){
                    $allcomment[$key]->profile_imagelink = asset('/users/avatar/');
                }elseif($data->gender == "male"){
                    $allcomment[$key]->profile_imagelink = asset('/users/avatar/');
                }

                $postedtime = $data->created_at;
                $differenttime = Carbon::parse($postedtime);
                $allcomment[$key]->postedtime = $differenttime->diffForHumans();
            }



            return response()->json(

                [

                    'message' => 'There is a data you need',

                    'post' => $singlePost,

                    'allcomment' => $allcomment

                ]

            );

        }catch(\Exception $e){

            return response()->json(['code' => $e->getCode(), 'message' => $e->getMessage()]);

        }



    }





    public function getAnsweredForumPost(){

        //jun forum post ma single matra vaye ni comment cha tyaslae dekhaune

    }



    public function getUnansweredForumPost(){

        //jun forum post ma euta pani comment chaina tyo dekhaune

    }



    public function getLatestForumPost(){

        //yo chai recent garera according to the timeline

    }



    public function getLoggedinUsersPost(){

        //yesma chai jun logged in vako user huncha tyasle gareko sabai post haru dekhaune with time in decending order

    }





    //comments haru

    public function getForumComment(){

        //jun pani forum post hucnha tyo sanga related comment haru tanne yesma

        //parameter haru chai--> user_id, forum post ko id

    }





    //function for event api

    public function  getAllEventPosts(){

        try{

            $response = EventTbl::getAllEvents();

            return response()->json(['message' => 'Operation successful', 'events' => $response, 'status' => 200]);



        }catch(\Exception $e){

            return response()->json([$e->getCode(), $e->getMessage()]);

        }



    }



    public function getSingleEventbyId($id){

        try{

            $response = EventTbl::getSpecificEventById($id);



            return response()->json(['message' => 'Operation successful for single event', 'events' => $response, 'status' => 200]);

        }catch(\Exception $e){

            return response()->json(['message' => $e->getMessage(), 'status' => $e->getCode()]);



        }

    }



    public function createEvent(Request $request){

        try{

            $event = new EventTbl();

            $event->event_title = $request->input('event_title');

            $event->event_description = $request->input('event_description');

            $event->start_datetime = $request->input('start_datetime');

            $event->event_location = $request->input('event_location');

            $event->event_image = $request->input('event_image');

            $event->end_datetime = $request->input('end_datetime');

            $event->end_datetime = $request->input('end_datetime');

            $event->user_id = $request->input('user_id');

            $event->status_id = 3;

            $event->created_at = Carbon::now();

            $event->updated_at = Carbon::now();



            $event->save();

            if($event->save()){

                return response()->json(['message' => 'Event created successfully', 'events' => $event]);

            }else{

                return response()->json(['message' => 'Sorry!!! Can not save event data ']);

            }

        }catch(\Exception $e){

            return response()->json(['message' => 'Something Went Wrong !! Error: '.$e->getMessage(), 'status' => $e->getCode()]);

        }

    }



    public function editEvent($id){

        try{

            $events = DB::table('event_tbl')

                ->join('users', 'event_tbl.user_id', '=', 'users.id')

                ->select('event_tbl.id as event_id', 'event_tbl.event_title', 'event_tbl.event_description',

                    'event_tbl.start_datetime', 'event_tbl.end_datetime', 'event_tbl.event_location',

                    'event_tbl.event_image', 'event_tbl.created_at', 'event_tbl.updated_at',

                    'users.username', 'users.id as user_id'

                )

                ->where('event_tbl.id', '=', $id)

                ->get();

            if(count($events) ==0){

                return response()->json(['message' => 'Can not find Event', 'status' => 404], 200);

            }else{

                return response()->json(['message' => 'Event Found', 'event' => $events]);

            }

        }catch(\Exception $e){

            return response()->json(['message' => 'Something Went Wrong !!! Error: '.$e->getMessage(), 'status' => $e->getCode()]);

        }

    }



    public function updateEvent(Request $request, $id){

        try{

            $updateEvent = new EventTbl();

//            $event->

        }catch(\Exception $e){



        }

    }



    public function getAllCategories(){

        try{

            $response = CategoryTbl::getAllCategory();

            return response()->json(['message' => 'list of all categories', 'categories' => $response, 200]);

        }catch(\Exception $e){

            return response()->json([$e->getCode(), $e->getMessage()]);

        }

    }



    /**

     * by aashish (this function returns all user activities related to forum post ant corresponding comments

     * @param $id

     * @return string

     */

    public function getAllUserActivities($user_id){



        try{

            // $results = PostTbl::all();
             $results = DB::table('post_tbl')
                ->join('users', 'post_tbl.user_id', '=' , 'users.id' )
                ->join('category_tbl', 'post_tbl.category_id', '=' , 'category_tbl.id' )
                ->join('userinfo_tbl', 'userinfo_tbl.user_id', '=' , 'users.id' )
                // ->join('comment_tbl', 'comment_tbl.post_id', '=', 'post_tbl.id')
                ->select(
                    'post_tbl.id as post_id','post_tbl.post_title','post_tbl.post_body','post_tbl.created_at', 'post_tbl.post_code',
                    'category_tbl.id as cat_id','category_tbl.category',
                    'users.username as username','users.email', 'users.id as user_id',
                    'userinfo_tbl.fname','userinfo_tbl.lname','userinfo_tbl.profile_image', 'userinfo_tbl.gender'
                )
                ->where('post_tbl.status_id', '=', '3')
                ->where('post_tbl.user_id', '=', $user_id)
                ->orderBy('created_at', 'DESC')
                ->get();

            $imageLocation = '';

            foreach($results as $key => $data){

                if($data->gender == "female"){
                    $results[$key]->profile_imagelink = asset('/users/avatar/');
                }elseif($data->gender == "male"){
                    $results[$key]->profile_imagelink = asset('/users/avatar/');
                }

                $postedtime = $data->created_at;
                $differenttime = Carbon::parse($postedtime);
                $results[$key]->postedtime = $differenttime->diffForHumans();
            }


            if(count($results) == 0){

                return response()->json(['message' => 'Nothing to show', 'status' => 503]);

            }else{

                return response()->json([
                    'message' => 'operation successfull', 
                    'datas' => $results,
                    'status' => 200
                    ]);

            }

        }catch(\Exception $e){

            return response()->json(['code' => $e->getCode(), 'message' => $e->getMessage()]);

        }


    }



    /**

     * by aashish

     * api function to post forum post

     * @param Request $request

     * @return array

     */

    public function insertForumPost(Request $request){

        try{

            $postObj = new PostTbl();

            $postObj->post_title = $request['post_title'];

            $postObj->post_body = $request['post_body'];

//            $postObj->post_image = $request['post_image'];

            $postObj->user_id = $request['user_id'];

            $postObj->post_code = $request['post_code'];

            $postObj->category_id = $request['category_id'];

            $postObj->status_id = 3; // status 3 i.e. unpublished

            $postObj->created_at = Carbon::now();

            $postObj->updated_at = Carbon::now();

            $postObj->save();



            if(!$postObj->save()){

                return array(['message' => 'Post can not be saved', 402]);

            }else{

                return array(['message' => 'forum post is successfully stored', 'posts' => $postObj]);

            }

        }catch(\Exception $e){

            return array(['message' => 'Something went wrong. Error code: '.$e->getMessage(), 'status' => $e->getCode()]);

        }

    }



    /**

     * by aashish

     * function to post comment of the post

     * @param Request $request

     * @return array

     */

    public function postComment(Request $request, $post_id){

        try{

            $commentObj = new CommentTbl();

            $commentObj->post_id = $post_id;

            $commentObj->user_id = $request->get('user_id');

            $commentObj->comment = $request->get('comment_body');

            $commentObj->comment_title = $request->get('comment_title');

            $commentObj->comment_status = "published";

            $commentObj->created_at = time();

            $commentObj->updated_at = time();

            $commentObj->save();

            if(!$commentObj->save()){

                return array(['message' => 'problem posting your comment']);

            }else{

                return array(['message' => 'comment created !!!', 'allcomment' => $commentObj]);

            }

        }catch(\Exception $e){

             return array(['message' => 'Someting went wrong !! Error: '.$e->getMessage(), 'status' => $e->getCode()]);

        }

    }

    public function getAllCommentsOfPosts($post_id){
        try{
            // return response(['message' => 'inside api']);
            $allcomment = CommentTbl::getCommentAccToPost($post_id);
            foreach($allcomment as $key => $data){

                if($data->gender == "female"){
                    $allcomment[$key]->profile_imagelink = asset('/users/avatar/');
                }elseif($data->gender == "male"){
                    $allcomment[$key]->profile_imagelink = asset('/users/avatar/');
                }

                $postedtime = $data->created_at;
                $differenttime = Carbon::parse($postedtime);
                $allcomment[$key]->postedtime = $differenttime->diffForHumans();
            }
            
            if(!$allcomment){
                return response(['message' => 'Comment not found', 'status' => 503]);
            }else{
               
                 return response(['message' => 'List of comments', 'allcomment' => $allcomment, 'status' => 200]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Someting went wrong !! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * by aashish
     * returns available programming languages
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getLanguages(){

        try{
            $result = DB::table('programming_language_tbl')
                ->select('id', 'programming_language')
                ->get();

            if(count($result) == 0){
                return response(['message' => 'No any course for now']);
            }else{
                return response(['message' => 'List of current programming languages', 'datas' => $result, 'status' => 200]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something Went Wrong !! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createFeedback(Request $request){
        try{
           
            $feedbackObj = new FeedbackTbl();
            $feedbackObj->created_at = Carbon::now();
            $feedbackObj->updated_at = Carbon::now();
            $imageFileName = $_FILES['file']['name'];

            $extentionArray = explode('.', $imageFileName);
            $ext = end($extentionArray);

            $imageFile = $_FILES['file']['tmp_name'];

            // $path = asset('/uploads/images/feedback/');
            $path = public_path('/uploads/images/feedback/');

            $filename = 'feedback_'.time().'.'.$ext;

            return response()->json([
                'message' => 'inside api', 
                'object' => $feedbackObj,
                'file ko name' => $filename,
                'extention ' => $ext,
                'public path' => $path
            ]);

            move_uploaded_file($filetmp, $path);

            // $imageFile->move($path , $filename);

            // $imageFile->move($path , $filename);

            // if($moveresult){
            //     return response()->json([
            //     'message' => 'inside api', 
            //     'object' => $feedbackObj,
            //     'file ko name' => $imageFileName,
            //     'extention ' => $ext,
            //     'public path' => $path
            // ]);
            // }else{
            //     return response(['message' => 'insert vayena image']);
            // }


            $feedbackObj->feedback_image = $filename;

            

            // $feedbackObj->title = $request->input('datas');
            // $feedbackObj->body = $request->get('feedback_detail');
            // $feedbackObj->username = $request->get('feedback_user');
            // $feedbackObj->feedback_image = $request->file('file');
            // $feedbackObj->created_at = Carbon::now();
            // $feedbackObj->updated_at = Carbon::now();
            // $feedbackObj->status = "incomplete";

            // return response(['message' => 'printing the request here ', 'request data ' => $feedbackObj, 'file desc' => $feedbackObj->feedback_image]);

            $feedbackObj->save();

            if($feedbackObj->save()){
                return response(['message' => 'new feedback is added', 'datas' => $feedbackObj, 'status' => 200]);
            }else{
                return response(['message' => 'Sorry !! Problem inserting your feedback', 'status' => 204]);
            }

        }catch(\Exception $e){
            return response(['message' => 'Something Went Wrong !! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getAllFeedback(){
        try{
            $result = DB::table('feedback_tbl')
                ->select('id', 'title', 'username', 'body', 'status', 'created_at', 'updated_at')
                ->orderBy('created_at', 'DESC')
                ->get();

            $countfeedback = count($result);

            foreach($result as $key => $data){
                $postedtime = $result[$key]->created_at;
                $differenttime = Carbon::parse($postedtime);
                $result[$key]->postedtime = $differenttime->diffForHumans();
            }

            if(count($result) == 0){
                return response(['message' => 'No any feedback for now']);
            }else{
                return response(['message' => 'List of feedback', 'datas' => $result, 'countfeedback' => $countfeedback, 'status' => 200]);
            }
        }catch(\Exception $e){
            return response(['message' => 'Something Went Wrong !! Error: '.$e->getMessage(), 'status' => $e->getCode()]);
        }
    }

}

