@component('mail::message')
![itglance cover image](https://scontent.fktm4-1.fna.fbcdn.net/v/t1.0-9/15542258_1294424170629114_9100817570042619222_n.jpg?oh=2d62f0041de17d5f7617f52965f1d3da&oe=594C5F3B)
##Congratulation !! your application is been approved.
Now you can login to our application using the credential below
    **Login Email** `{{ $users['loginemail'] }}`
    **Login Password   `{{ $users['loginpassword'] }}`

You can change your credential informtion in your profile section. [Click here](http://beta.itglance.org/users/profile)

@component('mail::button', ['url' => 'http://beta.itglance.org'])
Go to Application
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
